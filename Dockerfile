# use node 10
FROM node:10

# create app directory
WORKDIR /usr/src/app

#install app dependencies (copy package and package-lock.json)
COPY package*.json ./

# runs npm install
RUN npm install

# runs npm install for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# map port 3000 to docker daemon
EXPOSE 3000

# start node server
CMD [ "npm", "run", "start" ]
